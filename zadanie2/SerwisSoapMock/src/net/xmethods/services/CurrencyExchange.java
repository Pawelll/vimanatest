package net.xmethods.services;

import javax.jws.WebService;

@WebService(endpointInterface = "net.xmethods.services.CurrencyExchangePortType")
public class CurrencyExchange implements CurrencyExchangePortType {

  @Override
  public float getRate(String country1, String country2) {
    if (country1 != null && country2 != null) {
      return (country1.length() + country2.length()) * 0.75f;
    } else {
      return 1 / 3.0f;
    }
  }
}
