package net.xmethods.services;

import javax.xml.ws.Endpoint;

public class CurrencyExchangePublisher {

  public static void main(String[] args) {
    Endpoint.publish("http://localhost:80/soap", new CurrencyExchange());
  }
}
