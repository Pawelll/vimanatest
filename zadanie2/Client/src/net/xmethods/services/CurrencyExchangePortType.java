package net.xmethods.services;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface CurrencyExchangePortType {

  @WebMethod
  public float getRate(String country1, String country2);
}
