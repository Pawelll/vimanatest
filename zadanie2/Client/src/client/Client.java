package client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.xml.namespace.QName;

import javax.xml.ws.Service;
import net.xmethods.services.CurrencyExchangePortType;

public class Client {

  public static void main(String[] args) throws Exception {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    String country1 = reader.readLine();
    String country2 = reader.readLine();

    URL url = new URL("http://localhost:80/soap?wsdl");
    QName qname = new QName("http://services.xmethods.net/", "CurrencyExchangeService");
    Service service = Service.create(url, qname);
    qname = new QName("http://services.xmethods.net/", "CurrencyExchangePort");
    CurrencyExchangePortType currencyExchangePortType = service.getPort(qname, CurrencyExchangePortType.class);
    System.out.println(currencyExchangePortType.getRate(country1, country2));
  }
}
