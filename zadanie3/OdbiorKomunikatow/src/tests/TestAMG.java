package tests;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import odbiorkomunikatow.kolejka.IMessageQuerry;

public class TestAMG implements IMessageQuerry {

  @Override
  public Map<String, String> messageFromMQ() {
    Map<String, String> map = new ConcurrentHashMap<>();
    map.put("id", "AMG");
    map.put("name", "Mazowieckie");
    map.put("country", "Polska");
    return map;
  }
}
