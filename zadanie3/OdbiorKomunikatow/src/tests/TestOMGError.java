package tests;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import odbiorkomunikatow.kolejka.IMessageQuerry;

public class TestOMGError implements IMessageQuerry {

  @Override
  public Map<String, String> messageFromMQ() {
    Map<String, String> map = new ConcurrentHashMap<>();
    map.put("id", "OMG");
    //map.put("koszt", "2500");
    map.put("pojemnosc", "25.3");
    return map;
  }
}
