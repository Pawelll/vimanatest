package tests;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import odbiorkomunikatow.kolejka.IMessageQuerry;

public class TestASL implements IMessageQuerry {

  @Override
  public Map<String, String> messageFromMQ() {
    Map<String, String> map = new ConcurrentHashMap<>();
    map.put("id", "ASL");
    map.put("imie", "Jan");
    map.put("nazwisko", "Kowalski");
    return map;
  }
}
