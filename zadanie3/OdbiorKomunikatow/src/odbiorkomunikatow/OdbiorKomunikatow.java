package odbiorkomunikatow;

import java.util.Map;
import komunikat.factory.ObslugaKomunikatuFactory;
import odbiorkomunikatow.kolejka.IMessageQuerry;

public class OdbiorKomunikatow {

  public static void odbierzIObsluzKomunikat(IMessageQuerry messageQuerry) {
    Map<String, String> message = messageQuerry.messageFromMQ();
    try {
      ObslugaKomunikatuFactory.utworzKomunikat(message).obsluz();
    } catch (Exception e) {
      System.out.println("Error:");
      e.printStackTrace(System.out);
    }
  }
}
