package odbiorkomunikatow;

import odbiorkomunikatow.kolejka.IMessageQuerry;
import odbiorkomunikatow.kolejka.KolejkaWiadomosciPrzykladowa;

public class Start {

  public static void main(String[] args) {
    IMessageQuerry messageQuerry = new KolejkaWiadomosciPrzykladowa();
    //IMessageQuerry messageQuerry = new TestAMG();
    //IMessageQuerry messageQuerry = new TestASL();
    //IMessageQuerry messageQuerry = new TestOMG();
    //IMessageQuerry messageQuerry = new TestOMGError();
    OdbiorKomunikatow.odbierzIObsluzKomunikat(messageQuerry);
  }
}
