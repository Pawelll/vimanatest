package odbiorkomunikatow.kolejka;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class KolejkaWiadomosciPrzykladowa implements IMessageQuerry {

  @Override
  public Map<String, String> messageFromMQ() {
    Map<String, String> map = new ConcurrentHashMap<>();
    map.put("id", "AMG");
    map.put("name", "Alaska");
    map.put("country", "USA");
    return map;
  }
}
