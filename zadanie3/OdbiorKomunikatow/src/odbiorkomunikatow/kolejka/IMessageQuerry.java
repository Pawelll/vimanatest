package odbiorkomunikatow.kolejka;

import java.util.Map;

public interface IMessageQuerry {

  public Map<String, String> messageFromMQ();
}
