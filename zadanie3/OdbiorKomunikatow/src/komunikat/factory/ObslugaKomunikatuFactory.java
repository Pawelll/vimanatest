package komunikat.factory;

import java.util.Map;
import komunikat.obsluga.IKomunikat;
import komunikat.obsluga.InnyKomunikat;
import komunikat.obsluga.KomunikatAMG;
import komunikat.obsluga.KomunikatASL;
import komunikat.obsluga.KomunikatAST;
import komunikat.obsluga.KomunikatOMG;

public class ObslugaKomunikatuFactory {

  public static IKomunikat utworzKomunikat(Map<String, String> message) {
    switch (message.get("id")) {
      case "AST":
        return new KomunikatAST();
      case "ASL":
        return new KomunikatASL(message);
      case "AMG":
        return new KomunikatAMG(message);
      case "OMG":
        return new KomunikatOMG(message);
      default:
        return new InnyKomunikat();
    }
  }
}
