package komunikat.obsluga;

import java.util.Map;

public class KomunikatASL implements IKomunikat {

  private final String id;
  private final String imie;
  private final String nazwisko;

  public KomunikatASL(Map<String, String> message) {
    id = message.get("id");
    imie = message.get("imie");
    nazwisko = message.get("nazwisko");
  }

  @Override
  public void obsluz() {
    System.out.println(id + "   dane personalne: " + imie + " " + nazwisko);
  }
}
