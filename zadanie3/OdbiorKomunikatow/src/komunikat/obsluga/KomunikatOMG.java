package komunikat.obsluga;

import java.util.Map;

public class KomunikatOMG implements IKomunikat {

  private final String id;
  private final int koszt;
  private final double pojemnosc;

  public KomunikatOMG(Map<String, String> message) {
    id = message.get("id");
    koszt = Integer.parseInt(message.get("koszt"));
    pojemnosc = Double.parseDouble(message.get("pojemnosc"));
  }

  @Override
  public void obsluz() {
    System.out.println("Towar o pojemnosci " + pojemnosc + " l kosztuje " + koszt + " PLN");
  }
}
