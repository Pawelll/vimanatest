package komunikat.obsluga;

import java.util.Map;

public class KomunikatAMG implements IKomunikat {

  private final String id;
  private final String name;
  private final String country;

  public KomunikatAMG(Map<String, String> message) {
    id = message.get("id");
    name = message.get("name");
    country = message.get("country");
  }

  @Override
  public void obsluz() {
    System.out.println("AMG: " + id + ".\n" + name + " należy do " + country);
  }
}
